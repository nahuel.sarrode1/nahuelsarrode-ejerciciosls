const DYNAMODB = require('aws-sdk/clients/dynamodb');
const SNS = require('aws-sdk/clients/sns');

// instances
const dynamodb = new DYNAMODB.DocumentClient({ region: 'us-east-1' });
const sns = new SNS({ region: 'us-east-1' });

function calculateAge(birthday) {
	// birthday is a string in format YYYYMMDD
	const birthDate = new Date(birthday);
	const ageDifMs = Date.now() - birthDate.getTime();
	const ageDate = new Date(ageDifMs);

	return Math.abs(ageDate.getUTCFullYear() - 1970);
}

const pushSnsNotification = async (message) => {
	try {
		const paramsSNS = {
			Message: JSON.stringify(message),
			TopicArn: process.env.SnsTopic,
		};

		await sns.publish(paramsSNS).promise();
	} catch (error) {
		console.log(`Error pushing message into SNS: ${error}`);
		throw error;
	}
};

// save client into db
const saveClient = async (client) => {
	try {
		const dbParams = {
			TableName: process.env.candidates,
			Item: {
				dni: client.dni,
				name: client.name,
				lastnanme: client.lastname,
				date: client.date,
				email: client.email
			},
		};

		await dynamodb.put(dbParams).promise();
	} catch (error) {
		console.log(`Error saving user: ${error}`);
		throw error;
	}
};

exports.handler = async (event) => {
	try {
		let response;
		const body = JSON.parse(event.body);
		
		if (!body.name || !body.lastname || !body.dni || !body.date) {
			return response = {
				statusCode: 400,
				body: JSON.stringify('You must complete all fields'),
			};
		}
  
		if (calculateAge(body.date) > 65) {
			return response = {
				statusCode: 400,
				body: JSON.stringify('Users should have less than 65 years old'),
			};
		}

		await saveClient(body);
		await pushSnsNotification(body);

		return response = {
			statusCode: 200,
			body: JSON.stringify('User created successfully'),
		};

	} catch (error) {
		console.log(`Error saving user: ${error}`);
		throw error;
	}
};
